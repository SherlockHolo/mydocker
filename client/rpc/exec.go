package rpc

import (
	"context"
	"fmt"
	"io"
	"os"
	"os/signal"
	"syscall"
	"text/tabwriter"
	"text/template"

	"github.com/kr/pty"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/ssh/terminal"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

func ExecRun(conn *grpc.ClientConn, imageName, containerName, network, cmd string, stdin, stdout bool) (containerID string, err error) {
	client := NewExecClient(conn)

	resp, err := client.Run(context.Background(), &ExecRunReq{
		ContainerName: containerName,
		ImageName:     imageName,
		JoinNetwork:   network,
		Cmd:           cmd,
		Stdin:         stdin,
		Stdout:        stdout,
	})
	if err != nil {
		return "", xerrors.Errorf("send run req failed: %w", err)
	}

	if resp.Err != "" {
		return "", xerrors.Errorf("request run a container failed: %s", resp.Err)
	}

	return resp.ContainerId, nil
}

func ExecAttach(conn *grpc.ClientConn, containerID string, removeAfterAttach bool) error {
	client := NewExecClient(conn)

	attachClient, err := client.Attach(context.Background())
	if err != nil {
		return xerrors.Errorf("create attach client failed: %w", err)
	}

	var winSize *ExecWinSize

	if stdinSize, err := pty.GetsizeFull(os.Stdin); err == nil {
		winSize = &ExecWinSize{
			Rows: uint32(stdinSize.Rows),
			Cols: uint32(stdinSize.Cols),
			X:    uint32(stdinSize.X),
			Y:    uint32(stdinSize.Y),
		}
	}

	// handshake
	if err := attachClient.Send(&ExecAttachReq{
		ContainerId:       containerID,
		RemoveAfterAttach: removeAfterAttach,
		WinSize:           winSize,
	}); err != nil {
		return xerrors.Errorf("send attach handshake req failed: %w", err)
	}

	handshakeResp, err := attachClient.Recv()
	if err != nil {
		return xerrors.Errorf("recv attach handshake resp failed: %w", err)
	}

	if !handshakeResp.Exist {
		_ = attachClient.CloseSend()
		fmt.Printf("container %s doesn't exist\n", containerID)
		return nil
	}

	winSizeChangeCh := make(chan struct{}, 1)

	// handle signal
	signalCh := make(chan os.Signal, 1)

	if handshakeResp.Stdin {
		signal.Notify(signalCh, os.Interrupt, syscall.SIGWINCH)
	} else {
		signal.Notify(signalCh, syscall.SIGWINCH)
	}

	go func() {
		for s := range signalCh {
			switch s {
			case os.Interrupt:
				// ignore Ctrl+C.
				continue

			case syscall.SIGWINCH:
				winSizeChangeCh <- struct{}{}
			}
		}
	}()

	if handshakeResp.Stdin {
		oldState, err := terminal.MakeRaw(int(os.Stdin.Fd()))
		if err != nil {
			panic(err)
		}
		defer func() {
			_ = terminal.Restore(int(os.Stdin.Fd()), oldState)
		}()
	}

	go func() {
		buf := make([]byte, 10)

		for {
			n, err := os.Stdin.Read(buf)
			if err != nil {
				_ = attachClient.CloseSend()
				logrus.Error(xerrors.Errorf("read from stdin failed: %w", err))
				return
			}

			var winSize *ExecWinSize

			select {
			case <-winSizeChangeCh:
				if stdinSize, err := pty.GetsizeFull(os.Stdin); err == nil {
					winSize = &ExecWinSize{
						Rows: uint32(stdinSize.Rows),
						Cols: uint32(stdinSize.Cols),
						X:    uint32(stdinSize.X),
						Y:    uint32(stdinSize.Y),
					}
				}

			default:
			}

			if err := attachClient.Send(&ExecAttachReq{
				Data:    buf[:n],
				WinSize: winSize,
			}); err != nil {
				logrus.Error(xerrors.Errorf("send attach req failed: %w", err))
				_ = attachClient.CloseSend()
				return
			}
		}
	}()

	for {
		attachResp, err := attachClient.Recv()
		if xerrors.Is(err, io.EOF) {
			return nil
		}

		if err != nil {
			return xerrors.Errorf("recv attach resp failed: %w", err)
		}

		if _, err := os.Stdout.Write(attachResp.Data); err != nil {
			return xerrors.Errorf("write into stdout failed: %w", err)
		}
	}
}

func ExecList(conn *grpc.ClientConn, all bool) error {
	const listTemplate = "ID\tIMAGE NAME\tSTATUS\tCOMMAND\tCREATED\n{{range .}}{{.ID}}\t{{.ImageName}}\t{{.Status}}\t{{.Cmd}}\t{{.CreateTime}}\n{{end}}"

	type container struct {
		ID         string
		ImageName  string
		Status     string
		Cmd        string
		CreateTime string
	}

	client := NewExecClient(conn)

	listClient, err := client.List(context.Background(), &ExecListReq{All: all})
	if err != nil {
		return xerrors.Errorf("send list req failed: %w", err)
	}

	tmpl := template.Must(template.New("list").Parse(listTemplate))
	writer := tabwriter.NewWriter(os.Stdout, 0, 2, 4, ' ', tabwriter.TabIndent)
	defer writer.Flush()

	containers := make([]container, 0)

	for {
		resp, err := listClient.Recv()
		if err != nil {
			if xerrors.Is(err, io.EOF) {
				break
			}
			return xerrors.Errorf("recv list resp failed: %w", err)
		}

		containers = append(containers, container{
			ID:         resp.Id,
			ImageName:  resp.ImageName,
			Status:     resp.Status,
			Cmd:        resp.Cmd,
			CreateTime: resp.CreateTime,
		})
	}

	if err := tmpl.Execute(writer, containers); err != nil {
		return xerrors.Errorf("write containers info failed: %w", err)
	}
	return nil
}

func ExecStop(conn *grpc.ClientConn, containerID string) error {
	client := NewExecClient(conn)

	resp, err := client.Stop(context.Background(), &ExecStopReq{ContainerId: containerID})
	if err != nil {
		return xerrors.Errorf("send stop req failed: %w", err)
	}

	if !resp.Exist {
		fmt.Printf("container %s doesn't exist\n", containerID)
		return nil
	}

	if resp.Err != "" {
		return xerrors.Errorf("stop container %s failed: %s", containerID, resp.Err)
	}

	return nil
}

func ExecKill(conn *grpc.ClientConn, containerID string) error {
	client := NewExecClient(conn)

	resp, err := client.Kill(context.Background(), &ExecKillReq{ContainerId: containerID})
	if err != nil {
		return xerrors.Errorf("send kill req failed: %w", err)
	}

	if !resp.Exist {
		fmt.Printf("container %s doesn't exist\n", containerID)
		return nil
	}

	if resp.Err != "" {
		return xerrors.Errorf("kill container %s failed: %s", containerID, resp.Err)
	}

	return nil
}

func ExecRemove(conn *grpc.ClientConn, containerID string) error {
	client := NewExecClient(conn)

	resp, err := client.Remove(context.Background(), &ExecRemoveReq{ContainerId: containerID})
	if err != nil {
		return xerrors.Errorf("remove container failed: %w", err)
	}

	if !resp.Exist {
		fmt.Printf("container %s doesn't eixst\n", containerID)
		return nil
	}

	if resp.Err != "" {
		return xerrors.Errorf("remove container failed: %v", resp.Err)
	}

	return nil
}

func ExecExec(conn *grpc.ClientConn, containerID, cmd string) error {
	client := NewExecClient(conn)

	execClient, err := client.Exec(context.Background())
	if err != nil {
		return xerrors.Errorf("create exec client failed: %w", err)
	}

	var winSize *ExecWinSize

	if stdinSize, err := pty.GetsizeFull(os.Stdin); err == nil {
		winSize = &ExecWinSize{
			Rows: uint32(stdinSize.Rows),
			Cols: uint32(stdinSize.Cols),
			X:    uint32(stdinSize.X),
			Y:    uint32(stdinSize.Y),
		}
	}

	if err := execClient.Send(&ExecExecReq{
		ContainerId: containerID,
		Cmd:         cmd,
		WinSize:     winSize,
	}); err != nil {
		return xerrors.Errorf("send handshake exec req failed: %w", err)
	}

	resp, err := execClient.Recv()
	if err != nil {
		return xerrors.Errorf("recv handshake exec resp failed: %w", err)
	}

	if !resp.Exist {
		fmt.Printf("container %s doesn't exist\n", containerID)
		return nil
	}

	if resp.Err != "" {
		return xerrors.Errorf("exec into container %s failed: %v", containerID, resp.Err)
	}

	winSizeChangeCh := make(chan struct{}, 1)

	// handle signal
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGWINCH)
	go func() {
		for s := range signalCh {
			switch s {
			case os.Interrupt:
				// ignore Ctrl+C.
				continue

			case syscall.SIGWINCH:
				winSizeChangeCh <- struct{}{}
			}
		}
	}()

	oldState, err := terminal.MakeRaw(int(os.Stdin.Fd()))
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = terminal.Restore(int(os.Stdin.Fd()), oldState)
	}()

	go func() {
		buf := make([]byte, 10)

		for {
			n, err := os.Stdin.Read(buf)
			if err != nil {
				_ = execClient.CloseSend()
				logrus.Error(xerrors.Errorf("read from stdin failed: %w", err))
				return
			}

			var winSize *ExecWinSize

			select {
			case <-winSizeChangeCh:
				if stdinSize, err := pty.GetsizeFull(os.Stdin); err == nil {
					winSize = &ExecWinSize{
						Rows: uint32(stdinSize.Rows),
						Cols: uint32(stdinSize.Cols),
						X:    uint32(stdinSize.X),
						Y:    uint32(stdinSize.Y),
					}
				}

			default:
			}

			if err := execClient.Send(&ExecExecReq{
				Data:    buf[:n],
				WinSize: winSize,
			}); err != nil {
				_ = execClient.CloseSend()
				logrus.Error(xerrors.Errorf("send exec req failed: %w", err))
				return
			}
		}
	}()

	for {
		execResp, err := execClient.Recv()
		if xerrors.Is(err, io.EOF) {
			return nil
		}

		if err != nil {
			return xerrors.Errorf("recv exec resp failed: %w", err)
		}

		if _, err := os.Stdout.Write(execResp.Data); err != nil {
			return xerrors.Errorf("write into stdout failed: %w", err)
		}
	}
}
