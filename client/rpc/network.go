package rpc

import (
	"context"
	"io"
	"os"
	"text/tabwriter"
	"text/template"

	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

func NetworkCreate(conn *grpc.ClientConn, network, subnet string) error {
	client := NewNetworkClient(conn)

	resp, err := client.Create(context.Background(), &NetworkCreateReq{Name: network, Subnet: subnet})
	if err != nil {
		return xerrors.Errorf("grpc conn error: %w", err)
	}

	if resp.Err != "" {
		return xerrors.Errorf("create network failed: %v", resp.Err)
	}

	return nil
}

func NetworkRemove(conn *grpc.ClientConn, network string) error {
	client := NewNetworkClient(conn)

	resp, err := client.Delete(context.Background(), &NetworkDeleteReq{Name: network})
	if err != nil {
		return xerrors.Errorf("delete network failed: %w", err)
	}

	if resp.Err != "" {
		return xerrors.Errorf("delete network failed: %v", resp.Err)
	}

	return nil
}

func NetworkList(conn *grpc.ClientConn) error {
	const listTemplate = "NAME\tsubnet\n{{range .}}{{.Name}}\t{{.Subnet}}\n{{end}}"

	client := NewNetworkClient(conn)

	listClient, err := client.List(context.Background(), new(NetworkListReq))
	if err != nil {
		return xerrors.Errorf("create network list client failed: %w", err)
	}

	type network struct {
		Name   string
		Subnet string
	}

	nws := make([]network, 0)

LOOP:
	for {
		resp, err := listClient.Recv()
		switch {
		case xerrors.Is(err, io.EOF):
			break LOOP

		default:
			return xerrors.Errorf("recv network list resp failed: %w", err)

		case err == nil:
			nws = append(nws, network{
				Name:   resp.Name,
				Subnet: resp.Subnet,
			})
		}
	}

	tmpl := template.Must(template.New("network list").Parse(listTemplate))

	writer := tabwriter.NewWriter(os.Stdout, 0, 2, 4, ' ', tabwriter.TabIndent)
	defer writer.Flush()

	if err := tmpl.Execute(writer, nws); err != nil {
		return xerrors.Errorf("write network list result failed: %w", err)
	}

	return nil
}
