package rpc

import (
	"context"
	"os"
	"text/tabwriter"
	"text/template"

	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

func ImagesList(conn *grpc.ClientConn) error {
	client := NewImagesClient(conn)

	resp, err := client.List(context.Background(), new(ImageListReq))
	if err != nil {
		return xerrors.Errorf("grpc conn error: %w", err)
	}

	const tmplStr = "NAME\tSIZE\n{{range .}}{{.Name}}\t{{.Size}}\n{{end}}"
	tmpl := template.Must(template.New("images list").Parse(tmplStr))

	writer := tabwriter.NewWriter(os.Stdout, 0, 2, 4, ' ', tabwriter.TabIndent)
	defer writer.Flush()

	if err := tmpl.Execute(writer, resp.Images); err != nil {
		return xerrors.Errorf("display images failed: %w", err)
	}

	return nil
}

func ImagesImport(conn *grpc.ClientConn, imageName, imagePath string) error {
	client := NewImagesClient(conn)

	resp, err := client.Import(
		context.Background(),
		&ImageImportReq{
			Name: imageName,
			Path: imagePath,
		},
	)
	if err != nil {
		return xerrors.Errorf("grpc conn error: %w", err)
	}

	if resp.Err != "" {
		return xerrors.Errorf("import image failed: %v", resp.Err)
	}

	return nil
}
