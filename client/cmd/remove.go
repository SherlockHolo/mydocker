package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/client/rpc"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

var removeCmd = &cobra.Command{
	Use:   "rm [container id]",
	Short: "remove container",
	Args:  cobra.ExactArgs(1),
	Run: func(_ *cobra.Command, args []string) {
		containerID := args[0]

		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("grp dial failed: %w", err))
		}

		if err := rpc.ExecRemove(conn, containerID); err != nil {
			logrus.Fatal(err)
		}
	},
}
