package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/client/rpc"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

var networkCmd = &cobra.Command{
	Use:   "network",
	Short: "container network",
}

var createNetworkCmd = &cobra.Command{
	Use:   "create [network name] [subnet]",
	Short: "create a container network",
	Args:  cobra.ExactArgs(2),
	Run: func(_ *cobra.Command, args []string) {
		name, subnet := args[0], args[1]

		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("dial grpc failed: %w", err))
		}

		if err := rpc.NetworkCreate(conn, name, subnet); err != nil {
			logrus.Fatal(err)
		}
	},
}

var listNetworkCmd = &cobra.Command{
	Use:   "ls",
	Short: "list container networks",
	Args:  cobra.ExactArgs(0),
	Run: func(_ *cobra.Command, _ []string) {
		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("dial grpc failed: %w", err))
		}

		if err := rpc.NetworkList(conn); err != nil {
			logrus.Fatal(err)
		}
	},
}

var removeNetworkCmd = &cobra.Command{
	Use:   "rm [network]",
	Short: "remove container network",
	Args:  cobra.ExactArgs(1),
	Run: func(_ *cobra.Command, args []string) {
		name := args[0]

		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("dial grpc failed: %w", err))
		}

		if err := rpc.NetworkRemove(conn, name); err != nil {
			logrus.Fatal(err)
		}
	},
}
