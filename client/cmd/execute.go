package cmd

import "github.com/sirupsen/logrus"

func Execute() {
	rootCmd.InitDefaultVersionFlag()

	rootCmd.AddCommand(
		imagesCmd,
		networkCmd,
		runCmd,
		attachCmd,
		listCmd,
		stopCmd,
		killCmd,
		removeCmd,
		execCmd,
	)

	imagesCmd.AddCommand(
		imagesListCmd,
		imageImportCmd,
	)

	networkCmd.AddCommand(
		createNetworkCmd,
		removeNetworkCmd,
		listNetworkCmd,
	)

	runCmd.Flags().BoolVarP(&tty, "tty", "t", false, "allocate pseudo tty")
	runCmd.Flags().StringVar(&runNetwork, "net", "", "container network")
	runCmd.Flags().BoolVar(&removeAfterAttach, "rm", false, "remove container after run")
	runCmd.Flags().StringVarP(&containerName, "name", "n", "", "container name, if empty, will equal container id")
	runCmd.Flags().BoolVarP(&stdin, "interactive", "i", false, "keep stdin open")
	runCmd.Flags().BoolVarP(&detach, "detach", "d", false, "detach container")

	listCmd.Flags().BoolVarP(&listAllContainers, "all", "a", false, "list all containers include stopped or exited")

	if err := rootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
