package cmd

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/client/rpc"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

var (
	tty               bool
	runNetwork        string
	removeAfterAttach bool
	containerName     string
	stdin             bool
	detach            bool
)

var runCmd = &cobra.Command{
	Use:   "run [image name]  [cmd...]",
	Short: "run a container",
	Args:  cobra.MinimumNArgs(2),
	Run: func(_ *cobra.Command, args []string) {
		imageName := args[0]
		cmd := strings.Join(args[1:], " ")

		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("grp dial failed: %w", err))
		}

		containerID, err := rpc.ExecRun(conn, imageName, containerName, runNetwork, cmd, stdin, tty)
		if err != nil {
			logrus.Fatal(err)
		}

		if detach {
			fmt.Println(containerID)
			return
		}

		if err := rpc.ExecAttach(conn, containerID, removeAfterAttach); err != nil {
			logrus.Fatal(err)
		}
	},
}
