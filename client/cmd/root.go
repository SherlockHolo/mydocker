package cmd

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:     "mydocker",
	Short:   "mydocker client",
	Version: "0.2",
}
