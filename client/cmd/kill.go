package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/client/rpc"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

var killCmd = &cobra.Command{
	Use:   "kill [container id]",
	Short: "kill container",
	Args:  cobra.ExactArgs(1),
	Run: func(_ *cobra.Command, args []string) {
		id := args[0]

		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("grp dial failed: %w", err))
		}

		if err := rpc.ExecKill(conn, id); err != nil {
			logrus.Fatal(err)
		}
	},
}
