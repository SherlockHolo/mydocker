package cmd

import (
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/client/rpc"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

var execCmd = &cobra.Command{
	Use:   "exec [container id] [cmd]...",
	Short: "run a command into running container",
	Args:  cobra.MinimumNArgs(2),
	Run: func(_ *cobra.Command, args []string) {
		id := args[0]
		cmd := args[1:]

		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("grp dial failed: %w", err))
		}

		if err := rpc.ExecExec(conn, id, strings.Join(cmd, " ")); err != nil {
			logrus.Fatal(err)
		}
	},
}
