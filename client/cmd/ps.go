package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/client/rpc"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

var listAllContainers bool

var listCmd = &cobra.Command{
	Use:   "ps",
	Short: "list containers",
	Args:  cobra.ExactArgs(0),
	Run: func(_ *cobra.Command, _ []string) {
		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("grp dial failed: %w", err))
		}

		if err := rpc.ExecList(conn, listAllContainers); err != nil {
			logrus.Fatal(err)
		}
	},
}
