package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/client/rpc"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

var imagesCmd = &cobra.Command{
	Use:   "images",
	Short: "mydocker images",
}

var imagesListCmd = &cobra.Command{
	Use:   "list",
	Short: "list images",
	Args:  cobra.ExactArgs(0),
	Run: func(_ *cobra.Command, _ []string) {
		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("grp dial failed: %w", err))
		}

		if err := rpc.ImagesList(conn); err != nil {
			logrus.Fatal(err)
		}
	},
}

var imageImportCmd = &cobra.Command{
	Use:   "import [image name] [image path]",
	Short: "import image",
	Args:  cobra.ExactArgs(2),
	Run: func(_ *cobra.Command, args []string) {
		imageName, imagePath := args[0], args[1]

		conn, err := grpc.Dial("127.0.0.1:8888", grpc.WithInsecure())
		if err != nil {
			logrus.Fatal(xerrors.Errorf("grp dial failed: %w", err))
		}

		if err := rpc.ImagesImport(conn, imageName, imagePath); err != nil {
			logrus.Fatal(err)
		}
	},
}
