package paths

import "path/filepath"

const (
	DefaultMydockerDir = "/var/lib/mydocker"

	ConfigName = "config.json"
	UpperFile  = "diff"
	WorkFile   = "work"
	MergedFile = "merged"
	LogFile    = "container.log"
)

var (
	DefaultNetworkPath       = filepath.Join(DefaultMydockerDir, "network")
	IPAMDefaultAllocatorPath = filepath.Join(DefaultNetworkPath, "ipam/subnet.json")
	ImagesPath               = filepath.Join(DefaultMydockerDir, "images")
	RootFSPath               = filepath.Join(DefaultMydockerDir, "rootfs")
)
