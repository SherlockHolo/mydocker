package rpc

import (
	"context"

	"gitlab.com/SherlockHolo/mydocker/daemon/storage/errors"
	"gitlab.com/SherlockHolo/mydocker/daemon/storage/images"
	"golang.org/x/xerrors"
)

type ImagesRpc struct{}

func (ImagesRpc) Import(ctx context.Context, req *ImageImportReq) (*ImageImportResp, error) {
	if err := images.ImportImage(req.Path, req.Name); err != nil {
		imageImportConflict := new(errors.ImageImportConflict)

		if xerrors.As(err, imageImportConflict) {
			return &ImageImportResp{
				Err: imageImportConflict.Error(),
			}, nil
		}

		return &ImageImportResp{
			Err: xerrors.Errorf("import image failed: %w", err).Error(),
		}, nil
	}

	return &ImageImportResp{}, nil
}

func (ImagesRpc) List(ctx context.Context, req *ImageListReq) (*ImageListResp, error) {
	imgs, err := images.List()
	if err != nil {
		return nil, xerrors.Errorf("list all images failed: %w", err)
	}

	rpcImages := make([]*Image, 0, len(imgs))
	for _, img := range imgs {
		rpcImages = append(rpcImages, &Image{
			Name: img.Name,
			Size: img.Size,
		})
	}

	return &ImageListResp{Images: rpcImages}, nil
}

func (ImagesRpc) Delete(ctx context.Context, req *ImageDeleteReq) (*ImageDeleteResp, error) {
	if err := images.Delete(req.Name); err != nil {
		imageNotFound := new(errors.ImageNotFound)
		if xerrors.As(err, imageNotFound) {
			return &ImageDeleteResp{Err: imageNotFound.Error()}, nil
		}
		return &ImageDeleteResp{Err: xerrors.Errorf("delete image failed: %w", err).Error()}, err
	}

	return &ImageDeleteResp{}, nil
}
