package rpc

import (
	"context"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/SherlockHolo/mydocker/daemon/exec"
	"gitlab.com/SherlockHolo/mydocker/daemon/manager"
	"gitlab.com/SherlockHolo/mydocker/daemon/paths"
	"gitlab.com/SherlockHolo/mydocker/daemon/storage/errors"
	"gitlab.com/SherlockHolo/mydocker/daemon/storage/rootfs"
	"gitlab.com/SherlockHolo/mydocker/daemon/storage/volume"
	"gitlab.com/SherlockHolo/mydocker/info"
	"golang.org/x/xerrors"
)

type ExecRpc struct{}

func (ExecRpc) Run(ctx context.Context, req *ExecRunReq) (*ExecRunResp, error) {
	rootFS, err := rootfs.Create(req.ImageName)
	if err != nil {
		imageNotFound := new(errors.ImageNotFound)
		rootFSCreateConflict := new(errors.RootFSCreateConflict)

		switch {
		case xerrors.As(err, imageNotFound):
			return &ExecRunResp{Err: imageNotFound.Error()}, nil

		case xerrors.As(err, rootFSCreateConflict):
			return &ExecRunResp{Err: rootFSCreateConflict.Error()}, nil

		default:
			return &ExecRunResp{Err: xerrors.Errorf("create rootfs failed: %w", err).Error()}, nil
		}
	}

	if err := rootfs.Mount(rootFS); err != nil {
		return &ExecRunResp{Err: xerrors.Errorf("mount rootfs failed: %w", err).Error()}, nil
	}

	volumes, err := volume.Mount(rootFS.ID, req.Volumes)
	if err != nil {
		rootFSNotExist := new(errors.RootFSNotExist)
		volumeErr := new(errors.VolumeErr)

		switch {
		case xerrors.As(err, rootFSNotExist):
			return &ExecRunResp{Err: rootFSNotExist.Error()}, nil

		case xerrors.As(err, volumeErr):
			return &ExecRunResp{Err: volumeErr.Error()}, nil

		default:
			return &ExecRunResp{Err: xerrors.Errorf("mount volumes failed: %w", err).Error()}, nil
		}
	}

	cInfo, err := exec.Run(
		req.Cmd,
		rootFS,
		req.Envs,
		req.JoinNetwork,
		volumes,
		req.Stdin,
		req.Stdout,
	)
	if err != nil {
		return &ExecRunResp{Err: xerrors.Errorf("run container failed: %w", err).Error()}, nil
	}

	manager.AddContainer(cInfo.ID, cInfo)

	return &ExecRunResp{ContainerId: cInfo.ID}, nil
}

func (ExecRpc) Attach(attachServer Exec_AttachServer) error {
	// handshake
	handshakeReq, err := attachServer.Recv()
	if err != nil {
		return xerrors.Errorf("receive first attach req failed: %w", err)
	}

	cInfo, exist := manager.GetContainer(handshakeReq.ContainerId)
	if !exist {
		if err := attachServer.Send(&ExecAttachResp{Exist: false}); err != nil {
			return xerrors.Errorf("send attach first resp failed: %w", err)
		}
		return nil
	}

	if cInfo.Status != info.RUNNING {
		if err := attachServer.Send(&ExecAttachResp{Err: xerrors.Errorf("container %s is not running", cInfo.ID).Error()}); err != nil {
			return xerrors.Errorf("send attach first resp failed: %w", err)
		}
	}

	if err := attachServer.Send(&ExecAttachResp{Exist: true, Stdin: cInfo.StdinOpened}); err != nil {
		return xerrors.Errorf("send attach first resp failed: %w", err)
	}

	if ptmx, ok := cInfo.Stdin.(*os.File); ok {
		if err := exec.SetPtmxSize(
			ptmx,
			uint16(handshakeReq.WinSize.Rows),
			uint16(handshakeReq.WinSize.Cols),
			uint16(handshakeReq.WinSize.X),
			uint16(handshakeReq.WinSize.Y),
		); err != nil {
			logrus.Error(xerrors.Errorf("set ptmx win size failed: %w", err))
		}
	}

	defer func() {
		if handshakeReq.RemoveAfterAttach {
			<-cInfo.StoppedCtx.Done()

			if err := os.RemoveAll(filepath.Join(paths.RootFSPath, cInfo.RootFS.ID)); err != nil {
				logrus.Error(xerrors.Errorf("remove rootfs %s dir failed: %w", err))
			}

			manager.DeleteContainer(cInfo.ID)
		}
	}()

	go func() {
		for {
			req, err := attachServer.Recv()
			switch {
			case xerrors.Is(err, io.EOF):
				return

			default:
				logrus.Error(xerrors.Errorf("recv attach req failed: %w", err))
				return

			case err == nil:
				if req.WinSize != nil {
					if ptmx, ok := cInfo.Stdin.(*os.File); ok {
						if err := exec.SetPtmxSize(
							ptmx,
							uint16(handshakeReq.WinSize.Rows),
							uint16(handshakeReq.WinSize.Cols),
							uint16(handshakeReq.WinSize.X),
							uint16(handshakeReq.WinSize.Y),
						); err != nil {
							logrus.Error(xerrors.Errorf("set ptmx win size failed: %w", err))
						}
					}
				}

				if _, err := cInfo.Stdin.Write(req.Data); err != nil {
					logrus.Error(xerrors.Errorf("write to ptmx failed: %w", err))
					return
				}
			}
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	ch := make(chan []byte, 2)

	go func() {
		buf := make([]byte, 8192)
		for {
			n, err := cInfo.Stdout.Read(buf)
			if err != nil {
				cancel()
				return
			}
			select {
			case <-ctx.Done():
				return

			case ch <- buf[:n]:
			}
		}
	}()

	go func() {
		buf := make([]byte, 8192)
		for {
			n, err := cInfo.Stderr.Read(buf)
			if err != nil {
				cancel()
				return
			}
			select {
			case <-ctx.Done():
				return

			case ch <- buf[:n]:
			}
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return nil

		case data := <-ch:
			if err := attachServer.Send(&ExecAttachResp{Data: data}); err != nil {
				return xerrors.Errorf("send attach resp failed: %w", err)
			}
		}
	}
}

func (ExecRpc) List(req *ExecListReq, listServer Exec_ListServer) error {
	for _, cInfo := range manager.ListContainers(req.All) {
		if err := listServer.Send(&ExecListResp{
			Id:         cInfo.ID,
			ImageName:  cInfo.RootFS.ImageName,
			Status:     cInfo.Status,
			Cmd:        strings.Join(cInfo.Cmd, " "),
			CreateTime: cInfo.CreateTime.String(),
		}); err != nil {
			return xerrors.Errorf("send list resp failed: %w", err)
		}
	}

	return nil
}

func (ExecRpc) Stop(ctx context.Context, req *ExecStopReq) (*ExecStopResp, error) {
	cInfo, exist := manager.GetContainer(req.ContainerId)
	if !exist {
		return &ExecStopResp{Exist: false}, nil
	}

	if cInfo.Status == info.STOPPED {
		return &ExecStopResp{
			Exist:  true,
			Status: info.STOPPED,
		}, nil
	}

	if err := cInfo.Parent.Process.Signal(os.Interrupt); err != nil {
		return &ExecStopResp{
			Exist: true,
			Err:   xerrors.Errorf("stop container failed: %w", err).Error(),
		}, nil
	}

	return &ExecStopResp{
		Exist:  true,
		Status: info.STOPPED,
	}, nil
}

func (ExecRpc) Kill(ctx context.Context, req *ExecKillReq) (*ExecKillResp, error) {
	cInfo, exist := manager.GetContainer(req.ContainerId)
	if !exist {
		return &ExecKillResp{Exist: false}, nil
	}

	if cInfo.Status == info.STOPPED {
		return &ExecKillResp{
			Exist:  true,
			Status: info.STOPPED,
		}, nil
	}

	if err := cInfo.Parent.Process.Kill(); err != nil {
		return &ExecKillResp{
			Exist: true,
			Err:   xerrors.Errorf("stop container failed: %w", err).Error(),
		}, nil
	}

	return &ExecKillResp{
		Exist:  true,
		Status: info.STOPPED,
	}, nil
}

func (ExecRpc) Remove(ctx context.Context, req *ExecRemoveReq) (*ExecRemoveResp, error) {
	cInfo, exist := manager.GetContainer(req.ContainerId)
	if !exist {
		return &ExecRemoveResp{Exist: false}, nil
	}

	if cInfo.Status != info.STOPPED {
		return &ExecRemoveResp{Exist: true, Err: xerrors.Errorf("container %s is still running", cInfo.ID).Error()}, nil
	}

	manager.DeleteContainer(cInfo.ID)

	if err := rootfs.Delete(cInfo.ID); err != nil {
		return &ExecRemoveResp{Exist: true, Err: xerrors.Errorf("remove container %s rootfs failed: %w", cInfo.ID, err).Error()}, err
	}

	return &ExecRemoveResp{Exist: true}, nil
}

func (ExecRpc) Exec(execServer Exec_ExecServer) error {
	handshakeReq, err := execServer.Recv()
	if err != nil {
		return xerrors.Errorf("recv handshake exec req failed: %w", err)
	}

	cInfo, exist := manager.GetContainer(handshakeReq.ContainerId)
	if !exist {
		if err := execServer.Send(&ExecExecResp{Exist: false}); err != nil {
			return xerrors.Errorf("send handshake exec resp failed: %w", err)
		}
	}

	if cInfo.Status != info.RUNNING {
		if err := execServer.Send(&ExecExecResp{
			Exist: true,
			Err:   xerrors.Errorf("container %s doesn't running", cInfo.ID).Error(),
		}); err != nil {
			return xerrors.Errorf("send handshake exec resp failed: %w", err)
		}
	}

	ptmx, exist, err := exec.Exec(cInfo.ID, handshakeReq.Cmd)
	if err != nil {
		if err := execServer.Send(&ExecExecResp{
			Exist: true,
			Err:   xerrors.Errorf("exec into container %s failed: %w", cInfo.ID, err).Error(),
		}); err != nil {
			return xerrors.Errorf("send handshake exec resp failed: %w", err)
		}
	}

	if !exist {
		if err := execServer.Send(&ExecExecResp{Exist: false}); err != nil {
			return xerrors.Errorf("send handshake exec resp failed: %w", err)
		}
	}

	if err := execServer.Send(&ExecExecResp{Exist: true}); err != nil {
		return xerrors.Errorf("send handshake exec resp failed: %w", err)
	}

	if err := exec.SetPtmxSize(
		ptmx,
		uint16(handshakeReq.WinSize.Rows),
		uint16(handshakeReq.WinSize.Cols),
		uint16(handshakeReq.WinSize.X),
		uint16(handshakeReq.WinSize.Y),
	); err != nil {
		logrus.Error(xerrors.Errorf("set ptmx win size failed: %w", err))
	}

	go func() {
		for {
			req, err := execServer.Recv()
			switch {
			case xerrors.Is(err, io.EOF):
				return

			default:
				logrus.Error(xerrors.Errorf("recv exec req failed: %w", err))
				return

			case err == nil:
				if req.WinSize != nil {
					if err := exec.SetPtmxSize(
						ptmx,
						uint16(req.WinSize.Rows),
						uint16(req.WinSize.Cols),
						uint16(req.WinSize.X),
						uint16(req.WinSize.Y),
					); err != nil {
						logrus.Error(xerrors.Errorf("set ptmx win size failed: %w", err))
					}
				}

				if _, err := ptmx.Write(req.Data); err != nil {
					logrus.Error(xerrors.Errorf("write to ptmx failed: %w", err))
					return
				}
			}
		}
	}()

	buf := make([]byte, 8192)
	for {
		n, err := ptmx.Read(buf)
		if err != nil {
			return nil
		}

		if err := execServer.Send(&ExecExecResp{
			Exist: true,
			Data:  buf[:n],
		}); err != nil {
			return xerrors.Errorf("send exec resp failed: %w", err)
		}
	}
}
