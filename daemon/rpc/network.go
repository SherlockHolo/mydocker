package rpc

import (
	"context"

	"gitlab.com/SherlockHolo/mydocker/daemon/network"
	"golang.org/x/xerrors"
)

type NetworkRpc struct{}

func (NetworkRpc) Create(ctx context.Context, req *NetworkCreateReq) (*NetworkCreateResp, error) {
	if err := network.CreateNetwork("bridge", req.Subnet, req.Name); err != nil {
		return &NetworkCreateResp{Err: xerrors.Errorf("create network failed: %w", err).Error()}, nil
	}

	return &NetworkCreateResp{}, nil
}

func (NetworkRpc) Delete(ctx context.Context, req *NetworkDeleteReq) (*NetworkDeleteResp, error) {
	if err := network.DeleteNetwork(req.Name); err != nil {
		return &NetworkDeleteResp{Err: xerrors.Errorf("delete network failed: %w", err).Error()}, nil
	}

	return &NetworkDeleteResp{}, nil
}

func (NetworkRpc) List(_ *NetworkListReq, listServer Network_ListServer) error {
	networks := network.List()

	for _, nw := range networks {
		name := nw.Name()
		subnet := nw.Subnet()

		if err := listServer.Send(&NetworkListResp{
			Name:   name,
			Subnet: subnet.String(),
		}); err != nil {
			return xerrors.Errorf("send network list resp failed: %w", err)
		}
	}

	return nil
}
