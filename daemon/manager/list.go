package manager

import (
	"gitlab.com/SherlockHolo/mydocker/info"
)

func ListContainers(all bool) []*info.Info {
	cInfos := make([]*info.Info, 0, len(containerMap))

	for _, cInfo := range containerMap {
		if cInfo.Status == info.RUNNING || all {
			cInfos = append(cInfos, cInfo)
		}
	}

	return cInfos
}
