package manager

import (
	"gitlab.com/SherlockHolo/mydocker/info"
)

var (
	containerMap = make(map[string]*info.Info)
	// mutex        = sync.Mutex{}
)

func AddContainer(id string, cInfo *info.Info) {
	containerMap[id] = cInfo
}

func DeleteContainer(id string) (exist bool) {
	if _, ok := containerMap[id]; ok {
		delete(containerMap, id)
		return true
	}
	return false
}

func GetContainer(id string) (*info.Info, bool) {
	cInfo, ok := containerMap[id]
	return cInfo, ok
}
