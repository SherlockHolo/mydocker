package exec

import (
	"os"

	"github.com/kr/pty"
	"golang.org/x/xerrors"
)

func SetPtmxSize(ptmx *os.File, rows, cols, x, y uint16) error {
	if err := pty.Setsize(ptmx, &pty.Winsize{
		Rows: rows,
		Cols: cols,
		X:    x,
		Y:    y,
	}); err != nil {
		return xerrors.Errorf("set ptmx size failed: %w", err)
	}

	return nil
}
