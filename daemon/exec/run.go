package exec

import (
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/kr/pty"
	"github.com/sirupsen/logrus"
	"gitlab.com/SherlockHolo/mydocker/daemon/exec/internal/bufReadWriteCloser"
	"gitlab.com/SherlockHolo/mydocker/daemon/exec/internal/nonOpWriteCloser"
	"gitlab.com/SherlockHolo/mydocker/daemon/exec/internal/run"
	"gitlab.com/SherlockHolo/mydocker/daemon/network"
	"gitlab.com/SherlockHolo/mydocker/daemon/storage/rootfs"
	"gitlab.com/SherlockHolo/mydocker/daemon/storage/volume"
	"gitlab.com/SherlockHolo/mydocker/info"
	"golang.org/x/xerrors"
)

func Run(
	cmdStr string,
	rootFS *rootfs.RootFS,
	envs []string,
	net string,
	volumes []volume.Volume,
	stdinOpened bool,
	stdoutOpened bool,
) (*info.Info, error) {

	parent, wPipe, err := run.NewParentProcess(envs, rootFS)
	if err != nil {
		return nil, xerrors.Errorf("new parent process failed: %w", err)
	}
	defer func() {
		_ = wPipe.Close()
	}()

	stoppedCtx, stoppedCancel := context.WithCancel(context.Background())

	cmdResultCh := make(chan error, 1)

	cInfo := &info.Info{
		ID:          rootFS.ID,
		RootFS:      rootFS,
		Cmd:         strings.Split(cmdStr, " "),
		CreateTime:  info.CustomTime(time.Now()),
		Network:     net,
		Parent:      parent,
		CmdResultCh: cmdResultCh,
		Status:      info.RUNNING,
		Volumes:     volumes,
		StoppedCtx:  stoppedCtx,
	}

	var releaseResources func()

	if stdoutOpened {
		cInfo.StdoutOpened = true

		ptmx, ptms, err := pty.Open()
		if err != nil {
			return nil, xerrors.Errorf("open pty failed: %w", err)
		}

		defer func() {
			_ = ptms.Close()
		}()

		parent.Stdin = ptms
		parent.Stdout = ptms
		parent.Stderr = ptms

		lg := bufReadWriteCloser.New()
		go func() {
			_, _ = io.Copy(lg, ptmx)
			_ = lg.Close()
		}()

		cInfo.Stdout = lg
		cInfo.Stderr = lg

		if stdinOpened {
			cInfo.StdinOpened = true
			cInfo.Stdin = ptmx
		} else {
			cInfo.Stdin = &nonOpWriteCloser.NonOpWriteCloser{Writer: ioutil.Discard}
		}

		parent.SysProcAttr.Setctty = true
		parent.SysProcAttr.Setsid = true

		releaseResources = func() {
			_ = ptmx.Close()
		}

	} else {
		stdoutR, stdoutW, err := os.Pipe()
		if err != nil {
			return nil, xerrors.Errorf("create pipe failed: %w", err)
		}

		parent.Stdout = stdoutW
		cInfo.Stdout = stdoutR

		stderrR, stderrW, err := os.Pipe()
		if err != nil {
			return nil, xerrors.Errorf("create pipe failed: %w", err)
		}

		parent.Stderr = stderrW
		cInfo.Stderr = stderrR

		var stdinR, stdinW *os.File

		if stdinOpened {
			stdinR, stdinW, err = os.Pipe()
			if err != nil {
				return nil, xerrors.Errorf("create pipe failed: %w", err)
			}

			parent.Stdin = stdinR
			cInfo.Stdin = stdinW
		}

		releaseResources = func() {
			_ = stdoutW.Close()
			_ = stderrW.Close()
			_ = stdoutR.Close()
			_ = stderrR.Close()

			if stdinW != nil {
				_ = stdinW.Close()
				_ = stdinR.Close()
			}
		}
	}

	if err := parent.Start(); err != nil {
		releaseResources()
		return nil, xerrors.Errorf("start parent failed: %w", err)
	}

	cInfo.Pid = parent.Process.Pid

	// when container exit, clean and release resources
	go func() {
		defer stoppedCancel()

		cmdResultCh <- parent.Wait()

		if cInfo.Stdin != nil {
			_ = cInfo.Stdin.Close()
		}

		releaseResources()

		if err := rootfs.Unmount(rootFS); err != nil {
			logrus.Error(xerrors.Errorf("unmount container %s rootfs failed: %w", rootFS.ID, err))
			return
		}

		if err := volume.Unmount(rootFS.ID, volumes); err != nil {
			logrus.Error(xerrors.Errorf("unmount container %s volumes failed: %w", rootFS.ID, err))
		}

		if err := network.RemoveContainerFromNetwork(cInfo); err != nil {
			logrus.Error(xerrors.Errorf("remove container %s network failed: %w", rootFS.ID, err))
		}

		cInfo.Status = info.STOPPED
	}()

	if net != "" {
		if err := network.AddContainerIntoNetwork(net, cInfo); err != nil {
			return nil, xerrors.Errorf("add container into network failed: %w", err)
		}
	}

	// write info so that the init process can exec read cmd
	encoder := json.NewEncoder(wPipe)
	if err := encoder.Encode(cInfo); err != nil {
		return nil, xerrors.Errorf("write container info failed: %w", err)
	}

	return cInfo, nil
}

func InitProcess() error {
	return run.InitProcess()
}
