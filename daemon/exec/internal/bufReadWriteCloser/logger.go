package bufReadWriteCloser

import (
	"bytes"
	"io"
	"sync"
	"sync/atomic"
)

type Logger struct {
	buffer  bytes.Buffer
	maxSize int
	mutex   sync.Mutex
	block   chan struct{} // block read when buffer is empty
	pool    sync.Pool
	close   int32
}

func New() io.ReadWriteCloser {
	const maxSize = 4 * 1024 * 1024 * 1024

	return &Logger{
		buffer:  bytes.Buffer{},
		maxSize: maxSize,
		block:   make(chan struct{}, 1),
		pool: sync.Pool{
			New: func() interface{} {
				return make([]byte, 8192)
			},
		},
	}
}

func (l *Logger) Read(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, nil
	}

	for {
		l.mutex.Lock()
		n, _ = l.buffer.Read(p)
		l.mutex.Unlock()

		if n > 0 {
			return n, nil
		}

		if atomic.LoadInt32(&l.close) == 1 {
			return 0, io.EOF
		}

		<-l.block
	}
}

func (l *Logger) Write(p []byte) (n int, err error) {
	if atomic.LoadInt32(&l.close) == 1 {
		return 0, Closed
	}

	size := len(p)

	l.mutex.Lock()
	defer l.mutex.Unlock()

	for l.buffer.Len()+size > l.maxSize {
		tmpB := l.pool.Get().([]byte)
		_, _ = l.buffer.Read(tmpB)
		l.pool.Put(tmpB)
	}

	// cancel read block
	select {
	case l.block <- struct{}{}:
	default:
	}

	return l.buffer.Write(p)
}

func (l *Logger) Close() error {
	atomic.StoreInt32(&l.close, 1)

	// cancel read block
	select {
	case l.block <- struct{}{}:
	default:
	}

	return nil
}
