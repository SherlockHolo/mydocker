package nonOpWriteCloser

import "io"

type NonOpWriteCloser struct {
	io.Writer
}

func (wc *NonOpWriteCloser) Close() error {
	return nil
}
