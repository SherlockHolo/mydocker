package exec

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/kr/pty"
	_ "gitlab.com/SherlockHolo/mydocker/daemon/exec/internal/nsenter"
	"gitlab.com/SherlockHolo/mydocker/daemon/manager"
	"gitlab.com/SherlockHolo/mydocker/info"
	"golang.org/x/xerrors"
)

const (
	pidEnv = "mydocker_pid"
	cmdEnv = "mydocker_cmd"
)

func Exec(containerID string, cmdStr string) (ptmx *os.File, exist bool, err error) {
	cInfo, exist := manager.GetContainer(containerID)
	if !exist {
		return nil, false, nil
	}

	if cInfo.Status != info.RUNNING {
		return nil, false, xerrors.Errorf("container %s isn't running", containerID)
	}

	b, err := ioutil.ReadFile(filepath.Join("/proc", strconv.Itoa(cInfo.Pid), "environ"))
	if err != nil {
		return nil, false, xerrors.Errorf("read container %s envs failed: %w", containerID, err)
	}

	cmd := exec.Command("/proc/self/exe", []string{"exec", containerID, cmdStr}...)

	cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%d", pidEnv, cInfo.Pid))
	cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", cmdEnv, cmdStr))
	cmd.Env = append(cmd.Env, strings.Split(string(b), "\u0000")...)

	ptmx, err = pty.Start(cmd)
	if err != nil {
		return nil, false, xerrors.Errorf("start exec process failed: %w", err)
	}

	go func() {
		_ = cmd.Wait()
		_ = ptmx.Close()
	}()

	return ptmx, true, nil
}
