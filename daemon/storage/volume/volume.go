package volume

type Volume struct {
	HostDir      string
	ContainerDir string
}
