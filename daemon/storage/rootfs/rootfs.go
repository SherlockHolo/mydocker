package rootfs

import "math/rand"

const (
	idLength = 8
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

type RootFS struct {
	ID        string
	ImageName string
	LowerDir  string
	UpperDir  string
	WorkDir   string
	MergedDir string
}

func generateInfoID() string {
	b := make([]rune, idLength)
	for i := 0; i < idLength; i++ {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
