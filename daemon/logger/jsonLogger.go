package logger

import (
	"bufio"
	"bytes"
	"encoding/json"
	"io"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/fsnotify/fsnotify"
	"gitlab.com/SherlockHolo/mydocker/daemon/exec/internal/bufReadWriteCloser"
	"golang.org/x/xerrors"
)

type jsonLoggerMsg struct {
	Msg  []byte    `json:"msg"`
	Time time.Time `json:"time"`
}

type JsonLogger struct {
	f         *os.File
	scanner   *bufio.Scanner
	lastBuf   bytes.Buffer
	closed    int32
	closeOnce sync.Once
	watcher   *fsnotify.Watcher
}

func NewJsonLogger(f *os.File, writeOnly bool) (jl *JsonLogger, err error) {
	jl = new(JsonLogger)

	jl.f = f

	if !writeOnly {
		jl.scanner = bufio.NewScanner(f)

		jl.watcher, err = fsnotify.NewWatcher()
		if err != nil {
			return nil, xerrors.Errorf("new watcher failed: %w", err)
		}

		if err := jl.watcher.Add(f.Name()); err != nil {
			return nil, xerrors.Errorf("add file in watcher failed: %w", err)
		}
	}

	return jl, nil
}

func (jl *JsonLogger) Read(p []byte) (n int, err error) {
	for {
		if jl.lastBuf.Len() > 0 {
			return jl.lastBuf.Read(p)
		}

		if !jl.scanner.Scan() {
			select {
			case <-jl.watcher.Events:
				continue

			case err = <-jl.watcher.Errors:
				if err == nil {
					return 0, io.EOF
				}
				return 0, xerrors.Errorf("json file watcher error: %w", err)
			}
		}

		b := jl.scanner.Bytes()

		msg := new(jsonLoggerMsg)
		if err := json.Unmarshal(b, msg); err != nil {
			return 0, xerrors.Errorf("unmarshal log failed: %w", err)
		}

		if len(msg.Msg) > len(p) {
			n := copy(p, msg.Msg)
			jl.lastBuf.Write(msg.Msg[n:])
			return len(p), nil
		}

		return copy(p, msg.Msg), nil
	}
}

func (jl *JsonLogger) Write(p []byte) (n int, err error) {
	if atomic.LoadInt32(&jl.closed) == 1 {
		return 0, bufReadWriteCloser.Closed
	}

	msg := jsonLoggerMsg{
		Msg:  p,
		Time: time.Now(),
	}

	b, err := json.Marshal(msg)
	if err != nil {
		return 0, xerrors.Errorf("marshal log failed: %w", err)
	}
	b = append(b, '\n')

	if _, err := jl.f.Write(b); err != nil {
		return 0, xerrors.Errorf("write log to file failed: %w", err)
	}

	return len(p), nil
}

func (jl *JsonLogger) Close() (err error) {
	jl.closeOnce.Do(func() {
		atomic.StoreInt32(&jl.closed, 1)
		err = jl.f.Close()
	})

	return err
}
