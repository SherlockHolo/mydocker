package drivers

import (
	"net"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/vishvananda/netlink"
	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal"
	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal/nat"
	"gitlab.com/SherlockHolo/mydocker/info"
	"golang.org/x/xerrors"
)

type BridgeNetworkDriver struct{}

func (bd *BridgeNetworkDriver) Name() string {
	return "bridge"
}

func (bd *BridgeNetworkDriver) CreateNetwork(subnet *net.IPNet, name string) (*internal.Network, error) {
	nw := internal.NewNetwork(name, subnet, bd.Name())

	if err := bd.initBridge(nw); err != nil {
		return nil, xerrors.Errorf("init bridge failed: %w", err)
	}

	return nw, nil
}

func (bd *BridgeNetworkDriver) Delete(network *internal.Network) error {
	iface, err := netlink.LinkByName(network.Name())
	if err != nil {
		return xerrors.Errorf("get iface failed: %w", err)
	}

	if err := netlink.LinkSetDown(iface); err != nil {
		return xerrors.Errorf("set down iface failed: %w", err)
	}

	if err := netlink.LinkDel(iface); err != nil {
		return xerrors.Errorf("del iface failed: %w", err)
	}

	_, subnet, _ := net.ParseCIDR(network.IPRange.String())
	if err := nat.UnsetSNAT(network.Name(), subnet); err != nil {
		return xerrors.Errorf("unset SNAT failed: %w", err)
	}

	return nil
}

func (bd *BridgeNetworkDriver) AddEndpointIntoNetwork(network *internal.Network, endpoint *internal.Endpoint, containerInfo *info.Info) error {
	br, err := netlink.LinkByName(network.Name())
	if err != nil {
		return xerrors.Errorf("get br failed: %w", err)
	}

	linkAttrs := netlink.NewLinkAttrs()
	linkAttrs.Name = endpoint.ID[:5]

	linkAttrs.MasterIndex = br.Attrs().Index

	endpoint.Device = &netlink.Veth{
		LinkAttrs: linkAttrs,
		PeerName:  "cif-" + endpoint.ID[:5],
	}

	if err := netlink.LinkAdd(endpoint.Device); err != nil {
		return xerrors.Errorf("add endpoint device failed: %w", err)
	}

	if err := netlink.LinkSetUp(endpoint.Device); err != nil {
		return xerrors.Errorf("set up endpoint device failed: %w", err)
	}

	if err := setEndpointIPAndRoute(endpoint, containerInfo); err != nil {
		return xerrors.Errorf("set endpoint ip and route failed: %w", err)
	}

	return nil
}

func (bd *BridgeNetworkDriver) Disconnect(network *internal.Network, endpoint *internal.Endpoint) error {
	return nil
}

func (bd *BridgeNetworkDriver) initBridge(nw *internal.Network) error {
	if err := createBridgeInterface(nw.Name()); err != nil {
		return xerrors.Errorf("create bridge failed: %w", err)
	}

	gatewayIP := nw.IPRange

	if err := setInterfaceIP(nw.Name(), *gatewayIP); err != nil {
		return xerrors.Errorf("set interface gateway IP failed: %w", err)
	}

	if err := setInterfaceUP(nw.Name()); err != nil {
		return xerrors.Errorf("set bridge up failed: %w", err)
	}

	_, subnet, _ := net.ParseCIDR(nw.IPRange.String())
	if err := nat.SetSNAT(nw.Name(), subnet); err != nil {
		return xerrors.Errorf("setup iptables failed: %w", err)
	}

	return nil
}

func createBridgeInterface(bridgeName string) error {
	if _, err := net.InterfaceByName(bridgeName); err == nil {
		return nil
	}

	linkAttrs := netlink.NewLinkAttrs()
	linkAttrs.Name = bridgeName

	br := &netlink.Bridge{LinkAttrs: linkAttrs}

	if err := netlink.LinkAdd(br); err != nil {
		return xerrors.Errorf("add br %s failed: %w", bridgeName, err)
	}

	return nil
}

func setInterfaceIP(name string, ipNet net.IPNet) error {
	retries := 2

	var (
		iface netlink.Link
		err   error
	)

	for i := 0; i < retries; i++ {
		iface, err = netlink.LinkByName(name)
		if err != nil {
			logrus.Debugf("retrieving new bridge netlink link [ %s ]... retrying", name)
			time.Sleep(2 * time.Second)
			continue
		}
		break
	}
	if err != nil {
		return xerrors.Errorf("Abandoning retrieving the new bridge link from netlink, Run [ ip link ] to troubleshoot the error: %w", err)
	}

	addr := &netlink.Addr{IPNet: &ipNet}

	if err := netlink.AddrAdd(iface, addr); err != nil {
		return xerrors.Errorf("add addr to iface failed: %w", err)
	}
	return nil
}

func setInterfaceUP(name string) error {
	iface, err := netlink.LinkByName(name)
	if err != nil {
		return xerrors.Errorf("get bridge failed: %w", err)
	}

	if err := netlink.LinkSetUp(iface); err != nil {
		return xerrors.Errorf("set bridge up failed: %w", err)
	}

	return nil
}

func setEndpointIPAndRoute(ep *internal.Endpoint, containerInfo *info.Info) error {
	peerLink, err := netlink.LinkByName(ep.Device.PeerName)
	if err != nil {
		return xerrors.Errorf("get peer name failed: %w", err)
	}

	exitNetns, err := enterNetns(peerLink, containerInfo)
	if err != nil {
		return xerrors.Errorf("enter netns failed: %w", err)
	}
	defer exitNetns()

	interfaceIP := *ep.Network.IPRange
	interfaceIP.IP = ep.IP

	if err := setInterfaceIP(ep.Device.PeerName, interfaceIP); err != nil {
		return xerrors.Errorf("set container interface IP failed: %w", err)
	}

	if err := setInterfaceUP(ep.Device.PeerName); err != nil {
		return xerrors.Errorf("set up container interface failed: %w", err)
	}

	if err := setInterfaceUP("lo"); err != nil {
		return xerrors.Errorf("set up container lo failed: %w", err)
	}

	_, ipNet, _ := net.ParseCIDR("0.0.0.0/0")
	defaultRoute := &netlink.Route{
		LinkIndex: peerLink.Attrs().Index,
		Gw:        ep.Network.IPRange.IP,
		Dst:       ipNet,
	}

	if err := netlink.RouteAdd(defaultRoute); err != nil {
		return xerrors.Errorf("add default route failed: %w", err)
	}

	return nil
}
