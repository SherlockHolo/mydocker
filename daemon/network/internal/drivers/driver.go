package drivers

import (
	"net"

	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal"
	"gitlab.com/SherlockHolo/mydocker/info"
)

type Driver interface {
	Name() string
	CreateNetwork(subnet *net.IPNet, name string) (*internal.Network, error)
	Delete(network *internal.Network) error
	AddEndpointIntoNetwork(network *internal.Network, endpoint *internal.Endpoint, containerInfo *info.Info) error
	Disconnect(network *internal.Network, endpoint *internal.Endpoint) error
}

var Drivers = make(map[string]Driver)
