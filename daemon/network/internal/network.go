package internal

import (
	"encoding/json"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"

	"golang.org/x/xerrors"
)

type Network struct {
	name    string
	IPRange *net.IPNet
	driver  string
}

func (nw *Network) Name() string {
	return nw.name
}

func (nw *Network) Subnet() net.IPNet {
	_, subnet, _ := net.ParseCIDR(nw.IPRange.String())
	return *subnet
}

func (nw *Network) Driver() string {
	return nw.driver
}

func NewNetwork(name string, ipRange *net.IPNet, driver string) *Network {
	return &Network{
		name:    name,
		IPRange: ipRange,
		driver:  driver,
	}
}

func (nw *Network) Remove(dumpPath string) error {
	if err := os.Remove(filepath.Join(dumpPath, nw.Name())); err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return xerrors.Errorf("remove network config file failed: %w", err)
	}

	return nil
}

func (nw *Network) Dump(dumpPath string) error {
	if _, err := os.Stat(dumpPath); err != nil {
		if os.IsNotExist(err) {
			if err := os.MkdirAll(dumpPath, 0700); err != nil {
				return xerrors.Errorf("mkdir dump path %s failed: %w", dumpPath, err)
			}
		} else {
			return xerrors.Errorf("get dump path %s stat failed: %w", dumpPath, err)
		}
	}

	nwPath := filepath.Join(dumpPath, nw.Name())

	b, err := json.MarshalIndent(nw, "", "    ")
	if err != nil {
		return xerrors.Errorf("encode network config failed: %w", err)
	}

	if err := ioutil.WriteFile(nwPath, b, 0600); err != nil {
		return xerrors.Errorf("write network config failed: %w", err)
	}

	return nil
}

func LoadNetwork(loadPath string) (*Network, error) {
	b, err := ioutil.ReadFile(loadPath)
	if err != nil {
		return nil, xerrors.Errorf("read load file failed: %w", err)
	}

	nw := new(Network)
	if err := json.Unmarshal(b, nw); err != nil {
		return nil, xerrors.Errorf("decode network config file failed:% w", err)
	}

	return nw, nil
}
