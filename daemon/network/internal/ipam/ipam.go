package ipam

import (
	"encoding/json"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"gitlab.com/SherlockHolo/mydocker/daemon/paths"
	"golang.org/x/xerrors"
)

type IPAM struct {
	SubnetAllocatorPath string                   `json:"subnet_allocator_path"`
	SubnetMap           map[string]*subnetConfig `json:"subnet_map"`
	mutex               sync.Mutex               `json:"-"`
}

type subnetConfig struct {
	IPBitmap string `json:"ip_bitmap"`
}

var IPAllAllocator = &IPAM{SubnetAllocatorPath: paths.IPAMDefaultAllocatorPath, SubnetMap: make(map[string]*subnetConfig)}

func (ipam *IPAM) load() error {
	ipam.SubnetMap = make(map[string]*subnetConfig)

	if _, err := os.Stat(ipam.SubnetAllocatorPath); err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return xerrors.Errorf("get subnet allocator config stat failed: %w", err)
	}

	b, err := ioutil.ReadFile(ipam.SubnetAllocatorPath)
	if err != nil {
		return xerrors.Errorf("read subnet allocator config failed: %w", err)
	}

	if err := json.Unmarshal(b, &ipam.SubnetMap); err != nil {
		return xerrors.Errorf("decode subnet allocator config failed: %w", err)
	}

	return nil
}

func (ipam *IPAM) dump() error {
	dir, _ := filepath.Split(ipam.SubnetAllocatorPath)
	if _, err := os.Stat(dir); err != nil {
		if os.IsNotExist(err) {
			if err := os.MkdirAll(dir, 0700); err != nil {
				return xerrors.Errorf("mkdir subnet allocator config dir failed: %w", err)
			}
		} else {
			return xerrors.Errorf("get subnet allocator config dir stat failed: %w", err)
		}
	}

	b, err := json.MarshalIndent(ipam.SubnetMap, "", "    ")
	if err != nil {
		return xerrors.Errorf("encode subnet allocator config failed: %w", err)
	}

	if err := ioutil.WriteFile(ipam.SubnetAllocatorPath, b, 0600); err != nil {
		return xerrors.Errorf("write subnet allocator config failed: %w", err)
	}

	return nil
}

func (ipam *IPAM) Allocate(subnet *net.IPNet) (ip net.IP) {
	subnetStr := subnet.String()

	one, size := subnet.Mask.Size()

	ipam.mutex.Lock()
	defer ipam.mutex.Unlock()

	if _, ok := ipam.SubnetMap[subnetStr]; !ok {
		ipam.SubnetMap[subnetStr] = &subnetConfig{
			IPBitmap: strings.Repeat("0", 1<<uint(size-one)),
		}
	}

	for i := range ipam.SubnetMap[subnetStr].IPBitmap {
		if ipam.SubnetMap[subnetStr].IPBitmap[i] == '0' {
			alloc := []rune(ipam.SubnetMap[subnetStr].IPBitmap)
			alloc[i] = '1'
			ipam.SubnetMap[subnetStr].IPBitmap = string(alloc)

			ip = make(net.IP, net.IPv4len)
			copy(ip, subnet.IP)

			for j := 4; j > 0; j-- {
				ip[4-j] += uint8(i >> uint((j-1)*8))
			}

			ip[3]++
			break
		}
	}

	return
}

func (ipam *IPAM) Release(subnet *net.IPNet, ip net.IP) {
	releaseIP := net.ParseIP(ip.String())
	releaseIP = releaseIP.To4()
	releaseIP[3]--

	_, subnet, _ = net.ParseCIDR(subnet.String())

	var index int
	for j := 4; j > 0; j-- {
		index += int((releaseIP[j-1] - subnet.IP[j-1]) << uint((4-j)*8))
	}

	subnetStr := subnet.String()

	ipam.mutex.Lock()
	defer ipam.mutex.Unlock()

	alloc := []rune(ipam.SubnetMap[subnetStr].IPBitmap)
	alloc[index] = '0'
	ipam.SubnetMap[subnetStr].IPBitmap = string(alloc)

	return
}

func (ipam *IPAM) DeleteSubnet(subnet *net.IPNet) bool {
	ipam.mutex.Lock()
	defer ipam.mutex.Unlock()

	subnetStr := subnet.String()

	var (
		alloc *subnetConfig
		ok    bool
	)
	if alloc, ok = ipam.SubnetMap[subnetStr]; !ok {
		return true
	}

	if strings.LastIndex(alloc.IPBitmap, "1") > 0 {
		return false
	}

	delete(IPAllAllocator.SubnetMap, subnetStr)

	return true
}
