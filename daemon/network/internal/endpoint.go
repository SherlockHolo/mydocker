package internal

import (
	"net"

	"github.com/vishvananda/netlink"
)

type Endpoint struct {
	ID      string
	Device  *netlink.Veth
	IP      net.IP
	PortMap []string
	Network *Network
}
