package network

import (
	"io/ioutil"
	"path/filepath"
	"strconv"

	"github.com/coreos/go-iptables/iptables"
	"github.com/sirupsen/logrus"
	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal/drivers"
	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal/nat"
	"golang.org/x/xerrors"
)

func init() {
	bridgeDriver := new(drivers.BridgeNetworkDriver)
	drivers.Drivers[bridgeDriver.Name()] = bridgeDriver

	/*if _, err := os.Stat(paths.DefaultNetworkPath); err != nil {
		if os.IsNotExist(err) {
			if err := os.MkdirAll(paths.DefaultNetworkPath, 0700); err != nil {
				logrus.Fatal(xerrors.Errorf("mkdir -p network config path failed: %w", err))
			}
		} else {
			logrus.Fatal(xerrors.Errorf("get network config path stat failed: %w", err))
		}
	}

	if err := filepath.Walk(paths.DefaultNetworkPath, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		dir, _ := filepath.Split(path)
		if filepath.Dir(dir) != paths.DefaultNetworkPath {
			return nil
		}

		nw, err := internal.LoadNetwork(path)
		if err != nil {
			return xerrors.Errorf("load network config failed: %w", err)
		}

		networks[nw.Name] = nw

		return nil
	}); err != nil {
		logrus.Fatal(xerrors.Errorf("load all network config failed: %w", err))
	}*/

	if err := ioutil.WriteFile("/proc/sys/net/ipv4/ip_forward", []byte(strconv.Itoa(1)), 0644); err != nil {
		logrus.Fatal(xerrors.Errorf("set ip forward failed: %w", err))
	}

	var err error
	if nat.Iptables, err = iptables.New(); err != nil {
		logrus.Fatal(xerrors.Errorf("new iptables setter failed: %w", err))
	}

	for _, nw := range networks {
		path := filepath.Join("/proc/sys/net/ipv4/conf", nw.Name(), "route_localnet")
		if err := ioutil.WriteFile(path, []byte(strconv.Itoa(1)), 0644); err != nil {
			logrus.Fatal(xerrors.Errorf("enable bridge local nat failed: %w", err))
		}
	}

	return
}
