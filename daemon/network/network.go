package network

import (
	"fmt"
	"net"
	"sync"

	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal"
	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal/drivers"
	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal/ipam"
	"gitlab.com/SherlockHolo/mydocker/daemon/network/internal/nat"
	"gitlab.com/SherlockHolo/mydocker/info"
	"golang.org/x/xerrors"
)

type Network interface {
	Name() string
	Subnet() net.IPNet
	Driver() string
}

var (
	networks = make(map[string]*internal.Network)
	mutex    = sync.Mutex{}
)

func CreateNetwork(driver, subnetStr, name string) error {
	mutex.Lock()
	defer mutex.Unlock()

	if _, exists := networks[name]; exists {
		return CreateNetworkConflict{
			ExistName:   name,
			ExistSubnet: networks[name].IPRange.String(),
		}
	}

	_, subnet, err := net.ParseCIDR(subnetStr)
	if err != nil {
		return xerrors.Errorf("parse subnet failed: %w", err)
	}

	gatewayIP := ipam.IPAllAllocator.Allocate(subnet)

	subnet.IP = gatewayIP

	network, err := drivers.Drivers[driver].CreateNetwork(subnet, name)
	if err != nil {
		return xerrors.Errorf("create network failed: %w", err)
	}

	networks[name] = network

	/*if err := network.Dump(paths.DefaultNetworkPath); err != nil {
		return xerrors.Errorf("dump network config failed: %w", err)
	}*/

	return nil
}

func DeleteNetwork(networkName string) error {
	mutex.Lock()
	defer mutex.Unlock()

	network, ok := networks[networkName]
	if !ok {
		return xerrors.Errorf("no such network %s", networkName)
	}

	ipam.IPAllAllocator.Release(network.IPRange, network.IPRange.IP)

	if err := drivers.Drivers[network.Driver()].Delete(network); err != nil {
		return xerrors.Errorf("delete network failed: %w", err)
	}

	delete(networks, networkName)

	/*if err := network.Remove(paths.DefaultNetworkPath); err != nil {
		return xerrors.Errorf("remove network failed: %w", err)
	}*/

	return nil
}

func AddContainerIntoNetwork(networkName string, cInfo *info.Info) error {
	network, ok := networks[networkName]
	if !ok {
		return xerrors.Errorf("no such network: %s", networkName)
	}

	_, subnet, _ := net.ParseCIDR(network.IPRange.String())
	ip := ipam.IPAllAllocator.Allocate(subnet)

	ep := &internal.Endpoint{
		ID:      fmt.Sprintf("%s-%s", cInfo.ID, networkName),
		IP:      ip,
		Network: network,
		PortMap: cInfo.PortMap,
	}

	if err := drivers.Drivers[network.Driver()].AddEndpointIntoNetwork(network, ep, cInfo); err != nil {
		return xerrors.Errorf("connect endpoint to network failed: %w", err)
	}

	nat.SetPortMap(ep, cInfo)

	cInfo.Network = networkName
	ipNet := *network.IPRange
	ipNet.IP = ip
	cInfo.IPNet = ipNet

	return nil
}

func RemoveContainerFromNetwork(cInfo *info.Info) error {
	if cInfo.IPNet.String() == "<nil>" {
		return nil
	}

	_, subnet, err := net.ParseCIDR(cInfo.IPNet.String())
	if err != nil {
		return xerrors.Errorf("parse container subnet failed: %w", err)
	}

	ipam.IPAllAllocator.Release(subnet, cInfo.IPNet.IP)

	nat.UnsetPortMap(cInfo)

	return nil
}
