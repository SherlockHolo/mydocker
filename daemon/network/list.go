package network

func List() []Network {
	nws := make([]Network, 0, len(networks))
	for _, nw := range networks {
		nws = append(nws, nw)
	}

	return nws
}
