package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/daemon/exec"
)

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "init container precess run user's process in container. Don't call it outside",
	Args:  cobra.ExactArgs(0),
	RunE: func(_ *cobra.Command, _ []string) error {
		logrus.Info("init come on")
		return exec.InitProcess()
	},
}
