package cmd

import "github.com/sirupsen/logrus"

func Execute() {
	rootCmd.AddCommand(
		initCmd,
	)

	if err := rootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
