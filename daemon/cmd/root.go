package cmd

import (
	"net"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/SherlockHolo/mydocker/daemon/rpc"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
)

var rootCmd = &cobra.Command{
	Use:     "mydockerd",
	Short:   "mydocker daemon",
	Version: "0.2",
	Run: func(cmd *cobra.Command, args []string) {
		run()
	},
}

func run() {
	grpcServer := grpc.NewServer()

	rpc.RegisterImagesServer(grpcServer, new(rpc.ImagesRpc))
	rpc.RegisterNetworkServer(grpcServer, new(rpc.NetworkRpc))
	rpc.RegisterExecServer(grpcServer, new(rpc.ExecRpc))

	grpcListener, err := net.Listen("tcp", "127.0.0.1:8888")
	if err != nil {
		logrus.Fatal(xerrors.Errorf("grpc listen failed: %w", err))
	}

	if err := grpcServer.Serve(grpcListener); err != nil {
		logrus.Fatal(err)
	}
}
